package ru.t1.dkozyaikin.tm.command.project;

import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-remove-by-id";

    public static final String DESCRIPTION = "Remove project by id";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getProjectTaskService().removeProjectById(id);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
