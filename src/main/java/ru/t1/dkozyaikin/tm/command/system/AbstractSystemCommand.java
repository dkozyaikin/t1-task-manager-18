package ru.t1.dkozyaikin.tm.command.system;

import ru.t1.dkozyaikin.tm.api.service.ICommandService;
import ru.t1.dkozyaikin.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
